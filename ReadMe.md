FTP File Transfer 

A simple file transfer Java Application that transfers files to and from the 
server and client and vice versa, along with many other functions.

To start sharing place the files in the 'ClientFiles' or the 'ServerFiles' on the
root folder of the project.
Else you can remove the client of the server out of the project, remove the package
and run the java file in the normal console window. The respective folders will be
created in the same folder as that of the source file.

NOTE : Netbeans and other IDE's don't support the '\r' carriage return in their 
	   consoles, so use the normal progress bar, when running in the IDE's.
	   
Current Version : 1.1

Version History : 

1.0
-Initial Version

1.1
-Select files through the index in the list presented
-Show 'empty folder' message
-Delete file function fixed
