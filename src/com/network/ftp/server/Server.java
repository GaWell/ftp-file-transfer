/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.network.ftp.server;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.text.NumberFormat;

/**
 *
 * @author Ronnie
 * 
 * FTP File Transfer - Version 1.1
 * Server Version
 * 
 * Crappy Situation : 
 * As of now, I am able to send a file from the Server to the Client.
 * The details of the files on the Server are also being displayed, but the
 * problem I'm facing now is how to end the reading loop at the Client side, after 
 * getting the list of files
 * I can't close the input or output streams, cause that will also cause the socket
 * to be disconnected 
 * 
 * What the heck should I do with this &%#@$ program!! :-(
 * 
 * NOTE : Everything's fine now.... whew!!
 * 
 * ToDo List : 
 * 1) Show file transfer visually - Done
 * 2) When folder empty, show empty message - Done
 * 3) Show Last Modified date - X(Not going to do)
 * 4) Add some authentication service - Done (pretty lame though..)
 * 5) Delete file on server - Done
 * 6) Add Folder upload/download option
 * 7) Fix uploading files
 * 
 * Log:
 * 
 * 19th July 2012, 12:08 AM : I just got an idea...
 *                            Why not create two different sockets at 2 different ports
 *                            One for the file details, and the other for the actual file transfer
 *                            It might just work!!!
 * 
 * 20th July 2012, 11:59 AM : The two socket thing worked.... 
 *                            Last minute ideas always work well, and this one was no exception :-)
 *                            Anyway the client upload and download are now working.. now all that 
 *                            is needed is to show the actual flow of file, instead of just showing the 
 *                            end result
 *                            Also, major refactoring is required... a lot of mess is lying here and there...
 * 
 * 29th July 2012, 10:55 PM : Just when everything seemed fine.... we have another problem,
 *                            The upload option doesn't seem to close at the server's end 
 *                            Need to do something about that....
 * 
 */
public class Server {

    ServerSocket server;
    Socket dataConnection;
    Socket infoConnection;
    BufferedInputStream input;
    BufferedOutputStream output;
    FileInputStream fileInput;
    FileOutputStream fileOutput;
    String userReplies;
    
    String nameOfDirectory = "ServerFiles";
    String nameOfFile;
    String sizeOfFile;
    
    boolean fileTransferSuccessful = false;
    
    public void startRunning()
    {
        try {
            server = new ServerSocket(5241);
        } catch(IOException ie) {
            System.out.println("Error in creating server : "+ie.getLocalizedMessage());
        }
        
        while(true) {
            try {
                waitForConnection();
                setupStreams();
                sendListOfFiles();
            } catch(IOException io) {
                System.out.println("I/O Error occured : "+io.getLocalizedMessage());
            }
        }
    }
    
    private void waitForConnection() throws IOException
    {
        System.out.println("\nWaiting for the data connection : ");
        dataConnection = server.accept();
        infoConnection = server.accept();
        System.out.println("\nConnection Established with : "+dataConnection.getInetAddress().getHostAddress());
        System.out.println("*******************************************");

    }
    
    private void setupStreams() throws IOException
    {
        input = new BufferedInputStream(dataConnection.getInputStream());
        output = new BufferedOutputStream(dataConnection.getOutputStream());
        output.flush();
        System.out.println("/* - The streams have been set up - */");
        System.out.println("*******************************************");
    }
    
    private void sendListOfFiles() throws IOException
    {
        //Streams to send file details
        OutputStreamWriter sendList = new OutputStreamWriter(infoConnection.getOutputStream());
        BufferedReader userReply = new BufferedReader(new InputStreamReader(infoConnection.getInputStream()));
        
        //Make the directory if it doesn't exist..
        File directory = new File(nameOfDirectory);
        directory.mkdir();
        
        //String to display the size of the file
        String fileDetails;
        
        //Time to find what's in the box....
        File listOfFiles[] = directory.listFiles();
        
        //Make file sizes a little readadble...
        NumberFormat numberFormat = NumberFormat.getInstance();
        numberFormat.setMaximumFractionDigits(0);
        numberFormat.setGroupingUsed(true);
        
        System.out.println("\nThe current list of files in the Server are : ");
        System.out.println("---------------------------------------------------------");
        if(listOfFiles.length==0)
        {
            System.out.println("\t-------Nothin' in here-------");
        }
        for(int i=0;i<listOfFiles.length;i++)
        {
            fileDetails = (i+1)+"). "+listOfFiles[i].getName()+"------->"
                          +numberFormat.format(listOfFiles[i].length())+" bytes";
            System.out.println(fileDetails);
            sendList.write((fileDetails+"\n"));
        }

        System.out.println("---------------------------------------------------------");
        
        //To end the list of files and stop the loop at the client side..
        sendList.write("-1\n");
        sendList.flush();
        
        //Here, we receive the user's response
        userReplies = userReply.readLine();
        //System.out.println("Details received..");
        System.out.println("User's reply is : "+userReplies);
        if(userReplies.split(",")[0].equals("1"))
        {
            //This big statement is to find, the name of the file which the user chose...
            if(userReplies.split(",")[1].equals("-1"))
            {
                System.out.println("File not downloaded...");
                return;
            }
            nameOfFile = listOfFiles[Integer.parseInt(userReplies.split(",")[1])-1].getName();
            
            File file = new File(nameOfDirectory,nameOfFile);
            String size = String.valueOf(file.length());
            sendList.write(size+"\n");
            sendList.write(nameOfFile+"\n");
            sendList.flush();
            startSharingFiles(nameOfFile);
            System.out.println(userReply.readLine());
        }
        else if(userReplies.split(",")[0].equals("2"))
        {
            nameOfFile = userReplies.split(",")[1];
            if(nameOfFile.equals("-1"))
            {
                System.out.println("No File uploaded...");
                return;
            }
            sizeOfFile = userReply.readLine();
            startSharingFiles(nameOfFile);
            if(fileTransferSuccessful)
            {
                System.out.println("File has been uploaded : ");
                sendList.write("File has been transferred successfully..");
                sendList.flush();
            }
            else
            {
                System.out.println("Something went wrong while uploading....");
                sendList.write("Something went wrong while uploading....");
                sendList.flush();
            }
        }
        else if(userReplies.split(",")[0].equals("3"))
        {
            if(userReplies.split(",")[1].equals("-1"))
            {
                System.out.println("Nothing to delete...");
                return;
            }

            nameOfFile = listOfFiles[Integer.parseInt(userReplies.split(",")[1])-1].getName();
            
            File file = new File(nameOfDirectory,nameOfFile);
            if(file.delete())
            {
                sendList.write(file.getName()+"\n");
                System.out.println("File deleted..");
                sendList.write("File has been deleted successfully...\n");
                sendList.flush();
            }
            else
            {
                System.out.println("Something went wrong while deleting!!");
                sendList.write("Something went wrong while deleting!!\n");
                sendList.flush();
            }
        }
        
        
        //Successfully transferred and received info... yay!!
        infoConnection.close();
    }
    
    private void startSharingFiles(String fileName) throws IOException
    {
        //System.out.println(userChoice);
        int noOfChar;
        
        //userOptions contains the user's choice and the filename
        String userOptions[] = userReplies.split(",");

        byte Buffer[] = new byte[8192];//8192
        long lFileSize;
        long transferredBytes = 0;
        int starLimiter = 10;

        if(userOptions[0].equals("1"))
        {
            File file = new File(nameOfDirectory, fileName);
            //System.out.println("File exists : "+file.exists());
            lFileSize = file.length();
            fileInput = new FileInputStream(file);
            
            System.out.println();
            System.out.print("[- ");
            while((noOfChar = fileInput.read(Buffer))!=-1)
            {
                //System.out.println(noOfChar);
                
                //NetBeans and other IDE's don't seem to support the return carriage character... so, reverting to
                //old school progress bar
                transferredBytes+=noOfChar;
                while(((transferredBytes*100)/lFileSize)==starLimiter)
                {
                    System.out.print("* ");
                    starLimiter+=10;
                    break;
                }

                output.write(Buffer,0,noOfChar);
            }
            System.out.println("-]");
            
            output.flush();
            output.close();
            fileInput.close();
        }
        
        else if(userOptions[0].equals("2"))
        {
            File file = new File(nameOfDirectory, fileName);
            file.createNewFile();
            //System.out.println("File exists : "+file.exists());
            
            lFileSize = Long.parseLong(sizeOfFile);
            fileOutput = new FileOutputStream(file);
            
            System.out.println();
            System.out.print("[- ");
            while((noOfChar = input.read(Buffer))!=-1)
            {
                transferredBytes+=noOfChar;
                while(((transferredBytes*100)/lFileSize)==starLimiter)
                {
                    System.out.print("* ");
                    starLimiter+=10;
                    break;
                }
                
                fileOutput.write(Buffer,0,noOfChar);
            }
            System.out.println("-]");
            
            fileOutput.flush();
            fileOutput.close();

            fileTransferSuccessful = true;
        }
    }
    
    public static void main(String[] args)
    {
        Server server = new Server();
        server.startRunning();
    }
    
}