/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.Console;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.text.NumberFormat;

/**
 *
 * @author Ronnie
 * 
 * FTP File Transfer - Version 1.1
 * Client - Console Version
 * 
 * 
 */
public class ConsoleClient {

    Socket dataSocket;
    Socket infoSocket;
    BufferedInputStream input;
    BufferedOutputStream output;
    BufferedReader userInput;
    File clientDirectory;
    FileInputStream fileInput;
    FileOutputStream fileOutput;
    
    //Storing the file name to be sent or received
    String indexOfFile;
    String clientDirectoryName = "ClientFiles";
    String userChoice="0";
    String sizeOfFile;
    String nameOfFile;
    String userName;
    String setUserName = "username";
    String setPassword = "password";
    
    boolean fileTransferSuccessful = false;
    boolean accessGranted = false;
    char[] password = new char[20];
    
    public void startRunning()
    {
        {
            try {
                authenticateUser();
                if(accessGranted==true)
                {
                    attemptToConnect();
                    setupStreamsAndDirectories();
                    showCurrentFiles();
                }
            } catch(IOException io) {
                System.out.println("I/O Error occured : "+io.getLocalizedMessage());
            } finally {
                try {
                    closingStreams();
                } catch (IOException ex) {
                    System.out.println("Error in closing streams : "+ex.getLocalizedMessage());
                }
            }
        }
    }
    
    private void authenticateUser()
    {
        Console readUserDetails = System.console();
        int attempts = 0;
        System.out.println("\n---------------------------------------------------------");
        System.out.println("\t\t***USER AUTHENTICATION***");
        do
        {
            System.out.print("\nUsername : ");
            userName = readUserDetails.readLine();
            System.out.print("Password : ");
            password = readUserDetails.readPassword();
            if(userName.equals(setUserName) && setPassword.equals(new String(password)))
            {
                accessGranted = true;
                break;
            }
            else
            {
                System.out.println("Access Denied...");
            }
            attempts++;
        }while(attempts<3);
        if(accessGranted == false)
        {
            System.out.println("\n3 attempts made.... exiting program!!");
        }
        System.out.println("---------------------------------------------------------");
    }
    
    private void attemptToConnect() throws IOException
    {
        System.out.println("\nAttempting to Connect  : ");
        dataSocket = new Socket(InetAddress.getLocalHost(),5241);
        infoSocket = new Socket(InetAddress.getLocalHost(),5241);
        System.out.println("\nConnection Established with Server!!");
        System.out.println("*******************************************");

    }
    
    private void setupStreamsAndDirectories() throws IOException
    {
        input = new BufferedInputStream(dataSocket.getInputStream());
        output = new BufferedOutputStream(dataSocket.getOutputStream());
        output.flush();
        userInput = new BufferedReader(new InputStreamReader(System.in));
        
        //Creating the clientDirectory if it doesn't exist
        clientDirectory = new File(clientDirectoryName);
        clientDirectory.mkdir();

        System.out.println("/* - The streams have been set up - */");
        System.out.println("*******************************************");
    }
    
    private void showCurrentFiles() throws IOException
    {
        //To count the number of files
        int noOfFiles=0;
        
        //Getting the list of files..
        BufferedReader getList = new BufferedReader(new InputStreamReader(infoSocket.getInputStream()));
        OutputStreamWriter userReply = new OutputStreamWriter(infoSocket.getOutputStream());
        String message;
                
        System.out.println("");
        System.out.println("\nThe current list of files in the Server are : ");
        System.out.println("---------------------------------------------------------");
        while(!(message = getList.readLine()).equals("-1"))
        {
            System.out.println(message);
            noOfFiles++;
        }
        if(noOfFiles==0)
        {
            System.out.println("\t--------Nothin' in here--------");
        }
        
        System.out.println("---------------------------------------------------------");
        System.out.println("What would you like to do??");
        System.out.println("1. Download A File");
        System.out.println("2. Upload A File");
        System.out.println("3. Delete File On Server");
        System.out.print("\nEnter your choice : ");
        while(!userChoice.equals("1")&&!userChoice.equals("2")&&!userChoice.equals("3"))
        {
            userChoice = userInput.readLine();
            if(userChoice.equals("1"))
            {
                //If no files are present..
                if(noOfFiles==0)
                {
                    System.out.println("No files here...");
                    userReply.write(userChoice+","+"-1"+"\n");
                    userReply.flush();
                    break;
                }
                while(true)
                {
                    System.out.print("Enter the index of the file from the list : ");
                    indexOfFile = userInput.readLine();
                    int x = Integer.parseInt(indexOfFile);
                    if(x>0&&x<=noOfFiles)
                    {
                        break;
                    }
                    else
                    {
                        System.out.println("Dude.. WTF?? Enter the right option..\n");
                    }
                }
                userReply.write(userChoice+","+indexOfFile+"\n");
                userReply.flush();
                //System.out.println("Details sent..");
                sizeOfFile = getList.readLine();
                nameOfFile = getList.readLine();
                startSharingFiles(nameOfFile);
                if(fileTransferSuccessful)
                {
                    System.out.println("\nFile has been downloaded...");
                    userReply.write("File has been transferred successfully...\n");
                    userReply.flush();
                }
                else
                {
                    System.out.println("Something went wrong while downloading...");
                }
            }
            
            else if(userChoice.equals("2"))
            {
                nameOfFile = displayClientFiles(clientDirectory);
               
                userReply.write(userChoice+","+nameOfFile+"\n");
                userReply.flush();
               
                 if(nameOfFile.equals("-1"))
                {
                    System.out.println("Put some files in the 'ClientFiles' folder....");
                    break;
                }
                File file = new File(clientDirectory,nameOfFile);
                String size = String.valueOf(file.length());
                userReply.write(size+"\n");
                userReply.flush();
                
                startSharingFiles(nameOfFile);
                System.out.println(getList.readLine());
            }
            
            else if(userChoice.equals("3"))
            {
                if(noOfFiles==0)
                {
                    System.out.println("Nothing to delete");
                    userReply.write(userChoice+","+"-1"+"\n");
                    userReply.flush();
                    break;
                }
                while(true)
                {
                    System.out.print("Enter the index of the file from the list : ");
                    indexOfFile = userInput.readLine();
                    int x = Integer.parseInt(indexOfFile);
                    if(x>0&&x<=noOfFiles)
                    {
                        break;
                    }
                    else
                    {
                        System.out.println("Dude.. WTF?? Enter the right option..\n");
                    }
                }
                userReply.write(userChoice+","+indexOfFile+"\n");
                userReply.flush();
                System.out.println("\nFile selected : "+getList.readLine()+"\n");
                System.out.println(getList.readLine());
            }
            else
            {
                System.out.println("Come on man... are you blind?? Try again..");
            }
        }
        
        //Finally, the server understands me!!
        infoSocket.close();
    }
    
    private String displayClientFiles(File dir) throws IOException
    {
        String fileDetails;
        System.out.println("\nThe list of Files present in the Client Folder are : ");
        System.out.println("---------------------------------------------------------");            
                
        File listOfFiles[] = dir.listFiles();
        if(listOfFiles.length==0)
        {
            System.out.println("\t-------Nothin' in here--------");
            System.out.println("---------------------------------------------------------"); 
            return "-1";
        }
        
        int choice;
        NumberFormat numberFormat = NumberFormat.getInstance();
        numberFormat.setMaximumFractionDigits(0);
        numberFormat.setGroupingUsed(true);
            
        for(int i=0;i<listOfFiles.length;i++)
        {
            fileDetails = (i+1)+"). "+listOfFiles[i].getName()+"---->"
                    +numberFormat.format(listOfFiles[i].length())+" bytes";
            System.out.println(fileDetails);
        }
        System.out.println("---------------------------------------------------------"); 
        while(true)
        {
            System.out.print("Enter the index of the file to be sent : ");
            choice = Integer.parseInt(userInput.readLine());
            if(choice>0&&choice<=listOfFiles.length)
            {
                break;
            }
            else
            {
                System.out.println("Hey douche... do I have to teach you numbers as well??\n");
            }
        }
        return listOfFiles[choice-1].getName();
    }
    
    private void startSharingFiles(String fileName) throws IOException
    {
        //String indexOfFile;
        int noOfChar;
        byte Buffer[] = new byte[8192];//8192
        long transferredBytes = 0;
        long lFileSize;
        
        if(userChoice.equals("1"))
        {
            File file = new File(clientDirectory,fileName);
            file.createNewFile();
            lFileSize = Long.parseLong(sizeOfFile);
            
            System.out.println("\nFile selected : "+fileName);
            fileOutput = new FileOutputStream(file);
            System.out.println();
            while((noOfChar = input.read(Buffer))!=-1)
            {
                //System.out.println(noOfChar);

                transferredBytes+=noOfChar;
                System.out.print("\rFile Transfered : "+transferredBytes+" of "+sizeOfFile+" bytes--->"+(transferredBytes*100)/lFileSize+"% complete");
                
                fileOutput.write(Buffer,0,noOfChar);
            }
            System.out.print("\rFile Transfered : "+transferredBytes+" of "+sizeOfFile+" bytes--->"+(transferredBytes*100)/lFileSize+"% complete");

            fileOutput.flush();
            fileOutput.close();
            fileTransferSuccessful = true;
        }
        
        else if(userChoice.equals("2"))
        {   
            File file = new File(clientDirectory,fileName);
            //System.out.println("File exists : "+file.exists());
            fileInput = new FileInputStream(file);
            
            System.out.println("\nFile selected : "+fileName);
            lFileSize = file.length();
            System.out.println();
            while((noOfChar = fileInput.read(Buffer))!=-1)
            {
                transferredBytes+=noOfChar;
                System.out.print("\rFile Transfered : "+transferredBytes+" of "+lFileSize+" bytes--->"+(transferredBytes*100)/lFileSize+"% complete");

                output.write(Buffer,0,noOfChar);
            }
            System.out.print("\rFile Transfered : "+transferredBytes+" of "+lFileSize+" bytes--->"+(transferredBytes*100)/lFileSize+"% complete");

            System.out.println();
            output.flush();
            output.close();
            //fileInput.close();
        }
    }
    
    //Can't close output stream inside this function, because server will never know when the stream
    //ended, and it'll stay in the loop forever.....
    private void closingStreams() throws IOException
    {
        userInput.close();
        input.close();
    }
    
    public static void main(String[] args)
    {
        ConsoleClient client = new ConsoleClient();
        client.startRunning();
    }
}